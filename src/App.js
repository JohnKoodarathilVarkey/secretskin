import { StyledEngineProvider, ThemeProvider } from "@mui/material/styles";
import DashboardLayout from "./components/DashboardLayout";
import theme from "./theme";
//import "./App.css";

function App() {
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <DashboardLayout />
      </ThemeProvider>
    </StyledEngineProvider>
  );
}

export default App;
