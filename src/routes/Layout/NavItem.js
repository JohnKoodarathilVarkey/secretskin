import { useState } from "react";
// import {
//   NavLink as RouterLink,
//   matchPath,
//   useLocation,
// } from "react-router-dom";
import PropTypes from "prop-types";
import { Button, ListItem, Collapse, List } from "@mui/material";
import { ChevronDown, ChevronUp } from "react-feather";

const WithoutChildren = ({ href, icon: Icon, title, ...rest }) => {
  // const location = useLocation();

  const active = false;
  // href
  // ? !!matchPath(
  //     {
  //       path: href,
  //       end: false,
  //     },
  //     location.pathname
  //   )
  // : false;

  return (
    <ListItem
      disableGutters
      sx={{
        display: "flex",
        py: 0,
      }}
      {...rest}
    >
      <Button
        // component={RouterLink}
        sx={{
          color: "text.secondary",
          fontWeight: "medium",
          justifyContent: "flex-start",
          letterSpacing: 0,
          py: 1.25,
          textTransform: "none",
          width: "100%",
          ...(active && {
            color: "primary.main",
          }),
          "& svg": {
            mr: 1,
          },
        }}
        to={href}
      >
        {Icon && <Icon size="20" />}
        <span>{title}</span>
      </Button>
      <div style={{ flexGrow: 1 }} />
    </ListItem>
  );
};

const WithChildren = ({ icon: Icon, title, children, ...rest }) => {
  const [open, setOpen] = useState(false);
  const active = false;

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <>
      <ListItem
        disableGutters
        sx={{
          display: "flex",
          py: 0,
        }}
        {...rest}
      >
        <Button
          onClick={handleClick}
          sx={{
            color: "text.secondary",
            fontWeight: "medium",
            justifyContent: "flex-start",
            letterSpacing: 0,
            py: 1.25,
            textTransform: "none",
            width: "100%",
            ...(active && {
              color: "primary.main",
            }),
            "& svg": {
              mr: 1,
            },
          }}
        >
          {Icon && <Icon size="20" />}
          <span>{title}</span>
          <div style={{ flexGrow: 1 }} />
          {open ? <ChevronUp /> : <ChevronDown />}
        </Button>
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding style={{ marginLeft: "12px" }}>
          {children.map((item) => (
            <NavItem
              href={item.href}
              key={item.title}
              title={item.title}
              icon={item.icon}
              type="normal"
            >
              {item.children}
            </NavItem>
          ))}
        </List>
      </Collapse>
    </>
  );
};

const NavItem = ({ type, ...props }) => {
  switch (type) {
    case "group":
      return <WithChildren {...props} />;
    case "normal":
      return <WithoutChildren {...props} />;
    default:
      return <>Nav Item Error</>;
  }
};

NavItem.propTypes = {
  href: PropTypes.string,
  icon: PropTypes.elementType,
  title: PropTypes.string,
  type: PropTypes.string,
};

export default NavItem;
